docker stop deploykit-desktop
docker rm deploykit-desktop
docker rmi deploykit/desktop

docker build -t deploykit/desktop .
docker run -d -p 6080:8080 -p 6443:8443 -p 9990:9990 -p 6767:8787 --name deploykit-desktop deploykit/desktop
docker exec -t -i deploykit-desktop bash