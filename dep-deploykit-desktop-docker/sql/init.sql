use deploykit;

SELECT * FROM deploykit.t_groups;

SELECT * FROM deploykit.t_users;


insert into deploykit.t_user
	(label, uuid, password, username)
    values ('Administrator', 'UUID', 'jGl25bVBBBW96Qi9Te4V37Fnqchz/Eu4qB9vKrRIqRg=', 'admin');

insert into deploykit.t_role
	(label, uuid)
    values ('administrator', 'UUID');

insert into deploykit.t_role_user
	(role_id, user_id)
    values (1, 1);

insert into deploykit.t_group
	(label, uuid)
    values ('Pre-Production', 'UUID');

insert into deploykit.t_group_permission
	(allowed_to_deploy, allowed_to_edit, allowed_to_read, allowed_to_use, uuid, group_id, role_id)
    values (1, 1, 1, 1, 'UUID', 1, 1);
