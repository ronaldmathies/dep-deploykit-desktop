package nl.sodeso.deploykit.server.endpoint.session;

import nl.sodeso.gwt.ui.client.controllers.session.domain.LoginResult;
import nl.sodeso.gwt.ui.client.controllers.session.domain.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import java.security.Principal;

/**
 * @author Ronald Mathies
 */
@WebServlet(urlPatterns = {"*.session"})
public class SessionEndpoint extends nl.sodeso.gwt.ui.server.endpoint.session.SessionEndpoint {

    @Override
    public void onBeforeLoggingIn() {
    }

    @Override
    public void onAfterLoggedIn(LoginResult loginResult) {
    }

}
