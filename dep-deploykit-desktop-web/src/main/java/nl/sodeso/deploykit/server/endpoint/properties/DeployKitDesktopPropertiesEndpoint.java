package nl.sodeso.deploykit.server.endpoint.properties;

import nl.sodeso.deploykit.client.Constants;
import nl.sodeso.deploykit.client.properties.DeployKitDesktopClientApplicationProperties;
import nl.sodeso.deploykit.server.listener.DeployKitDesktopServerApplicationProperties;
import nl.sodeso.gwt.ui.server.endpoint.properties.AbstractApplicationPropertiesEndpoint;
import nl.sodeso.gwt.ui.server.endpoint.properties.ApplicationPropertiesContainer;

import javax.servlet.annotation.WebServlet;

/**
 * By default the application has an endpoint for properties consisting of the following settings:
 *
 * <ul>
 *  <li>name: Name of the application (used as the title)</li>
 *  <li>version: Version number of the application.</li>
 *  <li>devkitEnabled: Are the extra development tools available.</li>
 * </ul>
 *
 * If you would like to have more global application settings (not user specific), that don't
 * change during a session, then this is the place to add them. Just add the fields below
 * and fill them using the endpoint.
 *
 * @author Ronald Mathies
 */
@WebServlet(urlPatterns = {"*.deploykitdesktop-properties"})
public class DeployKitDesktopPropertiesEndpoint extends AbstractApplicationPropertiesEndpoint<DeployKitDesktopClientApplicationProperties, DeployKitDesktopServerApplicationProperties> {

    /**
     * {@inheritDoc}
     */
    @Override
    public Class<DeployKitDesktopClientApplicationProperties> getClientAppPropertiesClass() {
        return DeployKitDesktopClientApplicationProperties.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void fillApplicationProperties(DeployKitDesktopServerApplicationProperties serverAppProperties, DeployKitDesktopClientApplicationProperties clientApplicationProperties) {
        clientApplicationProperties.setName(Constants.MODULE_ID);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DeployKitDesktopServerApplicationProperties getServerAppProperties() {
        return ApplicationPropertiesContainer.instance().getApplicationProperties();
    }

}

