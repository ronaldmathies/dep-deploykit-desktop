package nl.sodeso.deploykit.server.listener;

import nl.sodeso.gwt.ui.server.endpoint.properties.ServerApplicationProperties;

/**
 * @author Ronald Mathies
 */
public class DeployKitDesktopServerApplicationProperties extends ServerApplicationProperties {

    public DeployKitDesktopServerApplicationProperties() {
        super();
    }
}
