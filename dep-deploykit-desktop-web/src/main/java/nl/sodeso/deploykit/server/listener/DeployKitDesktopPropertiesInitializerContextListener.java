package nl.sodeso.deploykit.server.listener;

import nl.sodeso.gwt.ui.server.listener.AbstractApplicationPropertiesInitializerContextListener;

import javax.servlet.annotation.WebListener;

/**
 * @author Ronald Mathies
 */
@WebListener
public class DeployKitDesktopPropertiesInitializerContextListener extends AbstractApplicationPropertiesInitializerContextListener<DeployKitDesktopServerApplicationProperties> {

    /**
     * {@inheritDoc}
     */
    @Override
    public Class<DeployKitDesktopServerApplicationProperties> getServerAppPropertiesClass() {
        return DeployKitDesktopServerApplicationProperties.class;
    }

}
